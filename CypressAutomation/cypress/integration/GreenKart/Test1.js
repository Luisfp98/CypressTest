/// <reference types="Cypress" />

describe('My First Test Suite', () => {
    it('My First Test Case', () => {
        //Test steps
        cy.visit('https://rahulshettyacademy.com/seleniumPractise/#/')
        cy.get('.search-keyword').type('ca')

        cy.wait(2000)
        //Locate only visible elements
        cy.get('.product:visible').should('have.length', 4)

        cy.get('.products').as('productLocator')
        cy.get('@productLocator').find('.product').should('have.length', 4)
        cy.get('@productLocator').find('.product').eq(2).contains('ADD TO CART').click().then(function () {
            console.log('test click')
        })

        cy.get('@productLocator').find('.product').each(($el, index, $list) => {
            const vegText = $el.find('h4.product-name').text()
            if (vegText.includes('Cashews')) {
                cy.wrap($el).contains('ADD TO CART').click()
            }
        })

        //Locate only visible elements
        // cy.get('.product:visible').should('have.length',4)
        // cy.get('.products').find('.product').should('have.length',4)
        // cy.get('.products').find('.product').eq(2).contains('ADD TO CART').click()

        // cy.get('.products').find('.product').each(($el,index,$list) =>{
        //   const vegText= $el.find('h4.product-name').text()
        //   if(vegText.includes('Cashews')){
        //       cy.wrap($el).contains('ADD TO CART').click()
        //   }
        // })

        //assert if logo tect is correctly displayed
        cy.get('.brand').should('have.text', 'GREENKART')

        //this is to print in logs
        cy.get('.brand').then(function (logoElement) {
            cy.log(logoElement.text())

        })
        //cy.log(logo.text())

    })

})