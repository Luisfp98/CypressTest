/// <reference types="Cypress" />

describe('API TESTING', ()=>{
    it('API test', ()=>{
        cy.request('POST','http://216.10.245.166/Library/Addbook.php',
        {
            "name":"Learn Appium Automation with Java 2",
            "isbn":"abcd",
            "aisle":"1227",
            "author":"John foe"
        }).then(function(response){
            expect(response.body).to.have.property('Msg','successfully added')
            expect(response.status).to.eq(200)
        })
    
        

    })

})