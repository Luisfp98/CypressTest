/// <reference types="Cypress" />
/// <reference types="cypress-iframe" />
import 'cypress-iframe'

describe('External Elements UI Explanation', ()=>{
    it('Test external tabs', ()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')

        cy.get('#opentab').then(function(el){
            const url = el.prop('href')
            cy.log(url)
            cy.visit(url)

        })
    })

    it('Test Frames', ()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')

        cy.frameLoaded('#courses-iframe')

        cy.iframe().find('a[href="#/mentorship"]').eq(0).click()

        cy.iframe().find('h1[class*="pricing-title"]').should('have.length',2)

    })
})