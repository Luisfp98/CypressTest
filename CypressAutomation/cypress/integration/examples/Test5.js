/// <reference types="Cypress" />

describe('External Elements UI Explanation', ()=>{
    it('Test web tables', ()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')

        cy.get('tr td:nth-child(2)')
            .each(($el,index,$list) =>{
                const text = $el.text()
                if(text.includes('Python')){
                    cy.get('tr td:nth-child(2)').eq(index).next().then(function(price){
                        expect(price.text()).to.equal('25') 
                    })
                }
        })
       
    })

    it('Test Hover element',()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')

        //cy.get('div.mouse-hover-content').invoke('show')
        cy.contains('Top').click({force:true})
        cy.url().should('include','top')
    })
})