/// <reference types="Cypress" />

describe('Elements UI Explanation', ()=>{
    it('Test checkbox',()=>{
        //Test steps
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.get('#checkBoxOption1').check().should('be.checked').and('have.value','option1')
        cy.get('#checkBoxOption1').uncheck().should('not.be.checked')

        cy.get('input[type="checkbox"]').check(['option2','option3']).should('be.checked')
    })

    it('Test dropdowns',()=>{
        //Test steps
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        
        //static dropdowns
        cy.get('select').select('option2').should('have.value','option2')

        //dynamic dropdowns
        cy.get('#autocomplete').type('cos')
        cy.get('.ui-menu-item-wrapper')
            .each(($el,index,$list) =>{
                if($el.text()=="Costa Rica"){
                 cy.wrap($el).click()
                }
            })
            .should('have.text','Costa Rica')

    })

    it('Test visibility',()=>{
        //Test steps
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')

        cy.get('#displayed-text').should('be.visible')
        cy.get('#hide-textbox').click()
        cy.get('#displayed-text').should('not.be.visible')
        cy.get('#show-textbox').click()
        cy.get('#displayed-text').should('be.visible')    
    })

    it('Test radio button',()=>{
        //Test steps
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.get('[value="radio2"]').check().should('be.checked')
    })
    
})