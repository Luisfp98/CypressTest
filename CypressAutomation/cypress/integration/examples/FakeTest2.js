/// <reference types="Cypress" />


describe('Mock HTTP Responses', ()=>{
    it('Test mock responses', ()=>{

        cy.visit('https://www.rahulshettyacademy.com/angularAppdemo/')

        cy.intercept('GET','https://rahulshettyacademy.com/Library/GetBook.php?AuthorName=shetty',(req)=>{
            req.url = 'https://rahulshettyacademy.com/Library/GetBook.php?AuthorName=malhotra'

            req.continue((res)=>{
                //expect(res.statusCode).to.equal(403)
            })
        }).as('dummyUrl')

        cy.get('button[class="btn btn-primary"]').click()
        cy.wait('@dummyUrl')

    })

})