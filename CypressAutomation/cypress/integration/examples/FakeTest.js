/// <reference types="Cypress" />


describe('Mock HTTP Responses', ()=>{
    it('Test mock responses', ()=>{

        cy.visit('https://www.rahulshettyacademy.com/angularAppdemo/')

        cy.intercept({
            method : 'GET',
            url : 'https://rahulshettyacademy.com/Library/GetBook.php?AuthorName=shetty'
        },
        {
            statusCode : 200,
            body : [
                {
                    "book_name": "RestAssured with Java",
                    "isbn": "RSU",
                    "aisle": "2301"
                }
            ]
        }).as('bookretrievals')
        
        cy.get('button[class="btn btn-primary"]').click()

        //length of the response array = rows from table
        cy.wait('@bookretrievals').should(({resquest,response})=>{
            cy.get('tr').should('have.length',response.body.length+1)
        })

        cy.get('p').should('have.text','Oops only 1 Book available')

        
        

    })

})