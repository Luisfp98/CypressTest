/// <reference types="Cypress" />

describe('External Elements UI Explanation', ()=>{
    it('Test alert box',()=>{
        //Test steps
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        cy.get('#alertbtn').click()
        cy.get('[value="Confirm"]').click()

        //Get pop up text
        cy.on('window:alert',(str)=>{
            //Mocha
            expect(str).to.equal('Hello , share this practice page and share your knowledge')
        })

        cy.on('window:confirm',(str)=>{
            //Mocha
            expect(str).to.equal('Hello , Are you sure you want to confirm?')
        })
    })

    it('Test external tabs', ()=>{
        cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        
        cy.get('#opentab').invoke('removeAttr','target').click()
        cy.url().should('include','index')
        cy.go('back')
    })
    

})