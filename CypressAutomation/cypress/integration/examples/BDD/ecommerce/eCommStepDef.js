/// <reference types="Cypress" />
import { Given,When,Then, And } from "cypress-cucumber-preprocessor/steps";
import HomePage from '../../../../support/pageObjects/HomePage'
import ProductPage from '../../../../support/pageObjects/ProductPage'

const homePage = new HomePage()
const productPage = new ProductPage()

let name 

Given('I open ECommerce Page', ()=>{
    cy.visit(Cypress.env('url')+'/angularpractice/')
})

When('I add Items to Cart',function(){
    homePage.getShopTab().click()
    this.data.productName.forEach(element => {
        cy.selectProduct(element)
    });
    Cypress.config('defaultCommandTimeout', 8000)
    productPage.getCheckout().click()
})

And('Validate the total prices',()=>{
    var sum = 0;
    cy.get('tr td:nth-child(4) strong').each(($el, index, $list) => {
        var res = $el.text().split(' ')[1].trim()
        sum = Number(sum) + Number(res)
    }).then(function () {
        cy.log(sum)
    })

    cy.get('h3 strong').then(function(element){
        var total = Number(element.text().split(' ')[1].trim())
        expect(total).to.equal(sum)
    })
})

Then('Select the Country submit and verify thanks message',()=>{
    cy.contains('Checkout').click()
    cy.get('#country').type('India')
    cy.get('.suggestions > ul > li > a').click()
    cy.get('#checkbox2').click({ force: true })
    cy.get('input[type="submit"]').click();
    cy.get('.alert').then(function(element){
            const actualText = element.text()
            expect(actualText.includes("Success")).to.be.true
        })
})


When('I fill the form details', function (dataTable) {
    name = dataTable.rawTable[1][0]
    homePage.getEditBox().type(dataTable.rawTable[1][0])
    homePage.getGender().select(dataTable.rawTable[1][1])
})

Then('I validate the form behavior', function () {
    homePage.getTwoWayDataBinding().should('have.value',name)
    homePage.getEditBox().should('have.attr', 'minlength', '2')
    homePage.getEntrepeneaur().should('be.disabled')
    Cypress.config('defaultCommandTimeout', 8000)
})

And('select the Shop Page',()=>{
    homePage.getShopTab().click()
})