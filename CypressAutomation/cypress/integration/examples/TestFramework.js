/// <reference types="Cypress" />
import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'

describe('Hooks', () => {

    before(function () {
        cy.fixture('example').then((data) => {
            this.data = data
        })
    })


    it('Practice', function () {
        const homePage = new HomePage()
        const productPage = new ProductPage()

        cy.visit(Cypress.env('url')+'/angularpractice/')

        homePage.getEditBox().type(this.data.name)
        homePage.getGender().select(this.data.gender)

        homePage.getTwoWayDataBinding().should('have.value', this.data.name)
        homePage.getEditBox().should('have.attr', 'minlength', '2')
        homePage.getEntrepeneaur().should('be.disabled')
        // cy.pause()

        homePage.getShopTab().click()

        this.data.productName.forEach(element => {
            cy.selectProduct(element)
        });
        // cy.selectProduct('Blackberry')
        //cy.get('#navbarResponsive > .navbar-nav > .nav-item > .nav-link').click()
        Cypress.config('defaultCommandTimeout', 8000)
        productPage.getCheckout().click()

        var sum = 0;
        cy.get('tr td:nth-child(4) strong').each(($el, index, $list) => {
            var res = $el.text().split(' ')[1].trim()
            sum = Number(sum) + Number(res)
        }).then(function () {
            cy.log(sum)
        })

        cy.get('h3 strong').then(function(element){
            var total = Number(element.text().split(' ')[1].trim())
            expect(total).to.equal(sum)
        })


        cy.contains('Checkout').click()
        cy.get('#country').type('India')
        cy.get('.suggestions > ul > li > a').click()
        cy.get('#checkbox2').click({ force: true })
        cy.get('input[type="submit"]').click();

        cy.get('.alert').should('contain.text', 'Success! Thank you! Your order will be delivered in next few weeks :-).')
        // cy.get('.alert').then(function(element){
        //     const actualText = element.text()
        //     expect(actualText.includes("Success")).to.be.true
        // })

    })

})